require_relative 'item_container'
class OutputWriter
  def validate_output_file(output_file_path)
    return check_outputfile_permissions(output_file_path) && file_exist(output_file_path)
  end
  def check_outputfile_permissions(output_file_path)
     return File.writable?( "output.csv" )
  end
  def file_exist(output_file_path)
    return File.exist?(output_file_path)
  end
  begin
  def write_file(output_file_path)
    if(validate_output_file(output_file_path))
      item_container_obj=ItemContainer.instance
      data=item_container_obj.get_data()
      CSV.open(output_file_path, "wb") do |csv|
      for i in 0..data.length-1
        csv << [data[i].get_s_no,data[i].get_name,data[i].get_price,data[i].get_country,data[i].get_tax]
      end
      end
    end  
  end
  rescue Exception => e
    raise e
  end
end
