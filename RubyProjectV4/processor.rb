require_relative 'item_container'
require_relative 'taxcalculator'
require_relative 'item'
class Processor
	tax_calculator_obj=TaxCalculator.new
	begin
	def get_sales_tax()
		item_container_obj=ItemContainer.instance
		data = item_container_obj.get_data()
		for i in 0..data.length-1
			price=data[i].get_price
			country=data[i].get_country
			data[i].calculate_tax(price,country)
		end
		item_container_obj.set_data(data)
	end
	rescue Exception => e
    raise e
  end
end
