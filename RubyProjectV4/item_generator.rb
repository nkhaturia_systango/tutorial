require_relative 'item'
require_relative 'item_container'
class ItemGenerator
	begin
	def generate_items(input_data_array)
		item_obj_array=[]
		for i in 0..input_data_array.length-1
		 	 item_obj_array[i]=Item.new
		end
		for i in 0..input_data_array.length-1
				s_no=input_data_array[i][0]
				name=input_data_array[i][1]
				price=input_data_array[i][2]
				country=input_data_array[i][3]
 				item_obj_array[i].set_s_no(s_no)
		 		item_obj_array[i].set_name(name)
		 		item_obj_array[i].set_price(price)
		 		item_obj_array[i].set_country(country)
		end
		item_container_obj=ItemContainer.instance
		item_container_obj.set_data(item_obj_array)
	end
	rescue Exception => e
    raise e
  end
end
