require 'singleton'
class ItemContainer
	include Singleton
	@singleton_item_obj_array
	def get_data()
		return @singleton_item_obj_array
	end
	def set_data(item_obj_array)
		@singleton_item_obj_array=item_obj_array
	end
end
