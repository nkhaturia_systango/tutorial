require_relative 'inputreader'
require_relative 'outputwriter'
require_relative 'processor'
input_reader_obj=InputReader.new
output_writer_obj=OutputWriter.new
processor_obj=Processor.new
input_file_path="input.csv"
output_file_path="output.csv"
begin
	input_reader_obj.read_file(input_file_path)
	processor_obj.get_sales_tax()
	output_writer_obj.write_file(output_file_path)
rescue Exception => e
	puts e.message
end