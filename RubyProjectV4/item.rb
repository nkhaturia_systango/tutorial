require_relative 'taxcalculator'
class Item
	@s_no
	@name
 	@price
 	@country
 	@tax
 		def get_s_no
 			return @s_no
 		end
		def get_name()
 			return @name
 		end 
 		def get_price()
 			return @price
 		end
 		def get_country()
 			return @country
 		end
		def get_tax()
 			return @tax
 		end
		def set_s_no(s_no)
 			@s_no=s_no
 		end
 		def set_name(name)
 			@name=name
 		end
 		def set_price(price)
 		    @price=price
 		end
 		def set_country(country)
 			@country=country
 		end
 		begin
 		def calculate_tax(price,country)
 			tax_calculator_obj=TaxCalculator.new
 			@tax=tax_calculator_obj.calculate_tax(price,country)
 		end
		rescue Exception => e
      raise e
    end
end