require_relative 'inputreader'
require_relative 'outputwriter'
require_relative 'processor'
input_reader_obj=InputReader.new
#taxcalculator_obj=TaxCalculator.new
output_writer_obj=OutputWriter.new
processor_obj=Processor.new
input_file_path="input.csv"
output_file_path="output.csv"
if input_reader_obj.check_inputfile_permissions(input_file_path) && input_reader_obj.file_exist(input_file_path)
	input_reader_obj.read_file(input_file_path)
	processor_obj.get_sales_tax()

	#for i in 0..input_csv_array.length - 1
  	#	tax[i]=taxcalculator_obj.output_array(input_csv_array[i][3],input_csv_array[i][2])
    #end
    if output_writer_obj.check_outputfile_permissions(output_file_path) && output_writer_obj.file_exist(output_file_path)
   		output_writer_obj.write_file()
	#else puts "outfile not found"
	end

else puts "input file not found"
end