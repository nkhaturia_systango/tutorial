require_relative 'taxcalculator'

class Item
	@s_no
	@name
 	@salary
 	@country
 	@tax
 		def get_s_no
 			return @s_no
 		end
		def get_name()
 			return @name
 		end 
 		def get_salary()
 			return @salary
 		end
 		def get_country()
 			return @country
 		end
		def get_tax()
 			return @tax
 		end


 		def set_s_no(s_no)
 			@s_no=s_no
 		end
 		def set_name(name)
 			@name=name
 		end
 		def set_salary(salary)
 		    @salary=salary
 		end
 		def set_country(country)
 			@country=country
 		end
 		def calculate_tax(salary,country)
 			tax_calculator_obj=TaxCalculator.new
 			if country.eql?("india")
      		@tax= tax_calculator_obj.tax_india(salary)
      	elsif country.eql?("usa")
      			 @tax= tax_calculator_obj.tax_usa(salary)
       			 elsif country.eql?("uk")
				    @tax= tax_calculator_obj.tax_uk(salary)
			    	else
			     	 @tax ="program does not support country"
 					end
end
end