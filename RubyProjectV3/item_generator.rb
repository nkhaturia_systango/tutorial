require_relative 'item'
require_relative 'item_container'
class ItemGenerator
	
	def generate_items(input_data_array)
		item_obj_array=[]
		for i in 0..input_data_array.length-1
		 	 item_obj_array[i]=Item.new
		end

 		for i in 0..input_data_array.length-1
 				item_obj_array[i].set_s_no(input_data_array[i][0])
		 		item_obj_array[i].set_name(input_data_array[i][1])
		 		item_obj_array[i].set_salary(input_data_array[i][2])
		 		item_obj_array[i].set_country(input_data_array[i][3])
		end
		#puts item_obj_array[0].get_s_no
		item_container_obj=ItemContainer.instance
		item_container_obj.set_data(item_obj_array)
	end
end
