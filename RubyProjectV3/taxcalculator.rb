class TaxCalculator
  def tax_india(salary)
    if(salary.to_i > 500)
      return 20+(salary.to_i-500)*20/100
    end
    if(salary.to_i>100 && salary.to_i<500)
      return (salary.to_i-100)*5/100
    end
    if(salary.to_i<100)
      return 0
    end
  end
  def tax_usa(salary)
    return Math.sqrt(salary.to_i)
  end
  def tax_uk(salary)
    return salary.to_i*3/100
  end
  def output_array(country,salary)
    if country.eql?("india")
      tax= tax_india(salary)
      elsif country.eql?("usa")
        tax= tax_usa(salary)
        elsif country.eql?("uk")
         tax= tax_uk(salary)
         else
          tax ="country not supported"
     return tax
    end
  end
end