require_relative 'item_generator'
class InputReader 
  
  require 'csv'
    def check_inputfile_permissions(input_file_path)
      return File.readable?( input_file_path )
    end
    def file_exist(input_file_path)
     return File.exist?(input_file_path)
    end
    def read_file(input_file_path)
      item_generator_obj = ItemGenerator.new
      input_data_array = CSV.read('input.csv')
      #puts input_data_array
      item_generator_obj.generate_items(input_data_array)
      
    end
end