class OutputWriter
  def check_outputfile_permissions(output_file_path)
     return File.writable?( "output.csv" )
  end
  def file_exist(output_file_path)
    return File.exist?(output_file_path)
  end
  def write_file(output_file_path,input_csv_array,tax)
   CSV.open("output.csv", "wb") do |csv|
      for i in 0..input_csv_array.length-1
          s_no = input_csv_array[i][0]
          name=input_csv_array[i][1]
          salary=input_csv_array[i][2]
          country=input_csv_array[i][3]
         csv << [s_no,name,salary,country,tax[i]]
         #csv << [s_no,tax[i]]
      end
    end
  end
end
