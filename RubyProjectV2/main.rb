require_relative 'inputreader'
require_relative 'taxcalculator'
require_relative 'outputwriter'
input_reader_obj=InputReader.new
taxcalculator_obj=TaxCalculator.new
outputclass_obj=OutputWriter.new
input_file_path="input.csv"
output_file_path="output.csv"
tax = []
if input_reader_obj.check_inputfile_permissions(input_file_path) && input_reader_obj.file_exist(input_file_path)
	input_csv_array = input_reader_obj.read_file(input_file_path)
	for i in 0..input_csv_array.length - 1
  		tax[i]=taxcalculator_obj.output_array(input_csv_array[i][3],input_csv_array[i][2])
    end
    if outputclass_obj.check_outputfile_permissions(output_file_path) && outputclass_obj.file_exist(output_file_path)
   		outputclass_obj.write_file(output_file_path,input_csv_array,tax)
	else puts "outfile not found"
	end
else puts "input file not found"
end