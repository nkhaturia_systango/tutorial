class InputReader 
  require 'csv'
    def check_inputfile_permissions(input_file_path)
      return File.readable?( input_file_path )
    end
    def file_exist(input_file_path)
     return File.exist?(input_file_path)
    end
    def read_file(input_file_path)
      input_data_array = CSV.read('input.csv')
      return input_data_array
    end
end